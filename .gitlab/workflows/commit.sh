#!/bin/bash

# SPDX-License-Identifier: BSD-3-Clause

git config --global --add safe.directory $(pwd)
git config user.name "Morello Project"
git config user.email "morello@morello-project.org"
git diff
git add -A
git status
git commit -m "morello: Update headers for ${RELEASE_NAME}"
git remote set-url --push origin "https://${CI_USER_AND_TOKEN}@git.morello-project.org/zdleaf/morello-linux-headers"
git push origin HEAD:morello/next
